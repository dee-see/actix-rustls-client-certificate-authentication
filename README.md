**This is resolved now** see the last version of the `make-certs.sh` file. I'm leaving this up if someone lands here in
their research. The reason for the issue is that `webpki` (the crate which rustls uses) only supports v3 x509 certificates.
You have to adapt your client certificate authentication accordingly. Huge thanks to [@d0nut](https://twitter.com/d0nutptr/)
for figuring this out!

---

# Reproduction of client cert issue

Execute `make-certs.sh` and then `cargo run`.

This works as expected thanks to `AllowAnyAuthenticatedClient`

```shell
$ curl -k https://localhost:8443/hey
curl: (56) OpenSSL SSL_read: OpenSSL/3.1.4: error:0A00045C:SSL routines::tlsv13 alert certificate required, errno 0
```

but when providing the certificate:

```shell
$ curl -k https://localhost:8443/hey --cert certs/client.crt --key certs/client.key 
curl: (56) OpenSSL SSL_read: OpenSSL/3.1.4: error:0A000410:SSL routines::sslv3 alert handshake failure, errno 0
```

doesn't and we get this server log

```text
[2023-11-13T11:29:07Z DEBUG rustls::server::hs] decided upon suite TLS13_AES_256_GCM_SHA384
[2023-11-13T11:29:07Z DEBUG rustls::server::hs] Chosen ALPN protocol [104, 50]
[2023-11-13T11:29:07Z WARN  rustls::conn] Sending fatal alert HandshakeFailure
```

What am I doing wrong?
