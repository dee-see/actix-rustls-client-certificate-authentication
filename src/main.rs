use std::{fs::File, io::BufReader};

use actix_web::{middleware::Logger, web, App, HttpResponse, HttpServer, Responder};
use rustls::{
    server::AllowAnyAuthenticatedClient, Certificate, PrivateKey, RootCertStore,
    ServerConfig,
};

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!\n")
}

#[actix_web::main]
pub async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::default().default_filter_or("debug"));

    HttpServer::new(move || {
        App::new()
            .route("/hey", web::get().to(manual_hello))
            .wrap(Logger::default())
    })
    .bind_rustls("0.0.0.0:8443", load_rustls_config())?
    .run()
    .await
}

fn load_rustls_config() -> ServerConfig {
    // load TLS key/cert files
    let cert_file =
        &mut BufReader::new(File::open("certs/server.crt").expect("Cannot open TLS cert file"));
    let key_file =
        &mut BufReader::new(File::open("certs/server.key").expect("Cannot open TLS key file"));

    let cert_chain: Vec<Certificate> = rustls_pemfile::certs(cert_file)
        .expect("Cannot parse server TLS cert file")
        .into_iter()
        .map(Certificate)
        .collect();

    let mut keys: Vec<PrivateKey> = rustls_pemfile::pkcs8_private_keys(key_file)
        .expect("Cannot parse server TLS key file")
        .into_iter()
        .map(PrivateKey)
        .collect();

    let ca_cert_file =
        &mut BufReader::new(File::open("certs/ca.crt").expect("Cannot open ca cert file"));
    let ca_cert: Vec<Certificate> = rustls_pemfile::certs(ca_cert_file)
        .expect("Cannot parse ca cert file")
        .into_iter()
        .map(Certificate)
        .collect();

    let mut cert_store = RootCertStore::empty();
    cert_store
        .add(&ca_cert[0])
        .expect("Failed to initialize cert store for client cert");

    let config_builder = ServerConfig::builder()
        .with_safe_defaults()
        .with_client_cert_verifier(AllowAnyAuthenticatedClient::new(cert_store))
        .with_single_cert(cert_chain, keys.remove(0));

    config_builder.expect("Invalid rustls config")
}
